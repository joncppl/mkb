//
// Created by jonathan on 23/04/22.
//

#pragma once

#include "KeyKind.hpp"
#include "Colour.hpp"

#include <array>
#include <ostream>
#include <istream>

namespace mkb
{

struct PerKeyState
{
  Colour colour {colours::black};

  // If and when non-static colour effects are implemented, their config will be here
};

struct KeyState
{
  KeyState() = default;

  [[nodiscard]] const PerKeyState& get_key_state(KeyKind kind) const
  {
    return m_state[static_cast<size_t>(kind)];
  }

  void set_key_state(KeyKind kind, const PerKeyState& state)
  {
    m_state[static_cast<size_t>(kind)] = state;
  }

  void set_all(const PerKeyState& state)
  {
    for (auto& key_state : m_state)
      key_state = state;
  }

private:
  using State = std::array<PerKeyState, num_key_kinds>;
  State m_state {};
};

struct KeyStateSerializer
{
public:
  void serialize(std::istream& is, const KeyState& state);

  void deserialize(std::istream& os, KeyState& state);

private:
};

}
