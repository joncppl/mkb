/// This util is just for creatin the key definitions from a byte-stream dump from SS's app
/// (it's not generally useful without modification)

#include "proto.hpp"

#include <filesystem>
#include <fstream>
#include <vector>

using namespace mkb;

static uint8_t parse_half_hex(char c)
{
    if (c >= 'a') return c - 'a' + 10;
    if (c >= 'A') return c - 'A' + 10;
    return c - '0';
}

static uint8_t parse_hex(const char* c)
{
    return parse_half_hex(*c) << 4 | parse_half_hex(*(c + 1));
}

void dump_parser(const std::filesystem::path& path)
{
    std::ifstream ifs(path);
    if (!ifs.is_open())
        abort();
    std::vector<uint8_t> data;
    data.emplace_back(0);
    while (!ifs.eof()) {
        char buf[2];
        ifs.read(buf, 2);
        data.emplace_back(parse_hex(buf));
    }

    const auto* gp = reinterpret_cast<const GP66::Packet*>(data.data());

    for (size_t i = 0; i < gp->keys.size(); i++) {
        const auto& key = gp->keys[i];
        printf("{0x%02x, %lu, (GP66::kb_region_t) 0x%02x, \"\"},\n", key.key_id, i, gp->kb_region);
    }
}

int main()
{
    dump_parser("./dumps/set_white/1");
    dump_parser("./dumps/set_white/2");
    dump_parser("./dumps/set_white/3");
    dump_parser("./dumps/set_white/4");

    return 0;
}