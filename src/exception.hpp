//
// Created by jonathan on 22/04/22.
//

#pragma once

#include <stdexcept>

#define DEFINE_RUNTIME_EXCEPTION(class_) \
  class class_ : public std::runtime_error \
  {                                      \
  public:                                \
    explicit class_(const char* msg) : runtime_error(msg) {} \
    explicit class_(const std::string& msg) : runtime_error(msg) {} \
  };                                     \

