//
// Created by jonathan on 22/04/22.
//

#pragma once

#include "proto.hpp"
#include "KeyKind.hpp"

#include <array>
#include <string_view>
#include <unordered_map>

namespace mkb
{

template<typename Proto>
struct KeyDefinition
{
  uint8_t key_id {};
  uint8_t spot_in_region {};
  typename Proto::kb_region_t region;
  std::string_view human_view;
   KeyKind key_kind;
};

template<typename Proto>
class KeyMap
{
  using Map = std::array<KeyDefinition<Proto>, num_key_kinds>;

public:
  template<size_t NumKeys>
  explicit KeyMap(const std::array<KeyDefinition<Proto>, NumKeys>& keys)
    : m_map(make_map(keys))
  {}

  [[nodiscard]] const KeyDefinition<Proto>& def(KeyKind kind) const
  {
    return m_map[static_cast<size_t>(kind)];
  }

private:
  Map m_map;

  template<typename Keys>
  static Map make_map(const Keys& keys)
  {
    Map ret {};
    for (size_t i = 0; i < keys.size(); i++) {
      if (keys[i].key_kind != KeyKind::None)
        ret[static_cast<size_t>(keys[i].key_kind)] = keys[i];
    }
    return ret;
  }
};

struct gp66_keys
{
  constexpr static std::array<KeyDefinition<GP66>, 42 * 4> keys {{
    {0x28, 0, (GP66::kb_region_t) 0x0b, ""}, // enter
    {0x31, 1, (GP66::kb_region_t) 0x0b, ""}, // backslack about enter
    {0x32, 2, (GP66::kb_region_t) 0x0b, ""}, //
    {0x64, 3, (GP66::kb_region_t) 0x0b, ""}, // backslash near right alt
    {0x87, 4, (GP66::kb_region_t) 0x0b, ""}, //
    {0x88, 5, (GP66::kb_region_t) 0x0b, ""}, //
    {0x89, 6, (GP66::kb_region_t) 0x0b, ""}, //
    {0x8a, 7, (GP66::kb_region_t) 0x0b, ""}, //
    {0x8b, 8, (GP66::kb_region_t) 0x0b, ""}, //
    {0x90, 9, (GP66::kb_region_t) 0x0b, ""}, //
    {0x91, 10, (GP66::kb_region_t) 0x0b, ""}, //
    {0x00, 11, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 12, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 13, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 14, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 15, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 16, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 17, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 18, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 19, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 20, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 21, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 22, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 23, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 24, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 25, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 26, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 27, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 28, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 29, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 30, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 31, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 32, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 33, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 34, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 35, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 36, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 37, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 38, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 39, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 40, (GP66::kb_region_t) 0x0b, ""},
    {0x00, 41, (GP66::kb_region_t) 0x0b, ""},

    {0x40, 0, (GP66::kb_region_t) 0x13, ""}, // F7
    {0x41, 1, (GP66::kb_region_t) 0x13, ""}, // F8
    {0x42, 2, (GP66::kb_region_t) 0x13, ""}, // F9
    {0x43, 3, (GP66::kb_region_t) 0x13, ""}, // F10
    {0x44, 4, (GP66::kb_region_t) 0x13, ""}, // F11
    {0x45, 5, (GP66::kb_region_t) 0x13, ""}, // F12
    {0x46, 6, (GP66::kb_region_t) 0x13, ""}, //
    {0x47, 7, (GP66::kb_region_t) 0x13, ""}, //
    {0x48, 8, (GP66::kb_region_t) 0x13, ""}, //
    {0x49, 9, (GP66::kb_region_t) 0x13, ""}, // delete
    {0x4a, 10, (GP66::kb_region_t) 0x13, ""}, // home
    {0x4b, 11, (GP66::kb_region_t) 0x13, ""}, // pgup
    {0x4c, 12, (GP66::kb_region_t) 0x13, ""}, //
    {0x4d, 13, (GP66::kb_region_t) 0x13, ""}, // end
    {0x4e, 14, (GP66::kb_region_t) 0x13, ""}, // pgdn
    {0x4f, 15, (GP66::kb_region_t) 0x13, ""}, // right
    {0x50, 16, (GP66::kb_region_t) 0x13, ""}, // left
    {0x51, 17, (GP66::kb_region_t) 0x13, ""}, // down
    {0x52, 18, (GP66::kb_region_t) 0x13, ""}, // up
    {0x00, 19, (GP66::kb_region_t) 0x13, ""},
    {0x00, 20, (GP66::kb_region_t) 0x13, ""},
    {0x00, 21, (GP66::kb_region_t) 0x13, ""},
    {0x00, 22, (GP66::kb_region_t) 0x13, ""},
    {0x00, 23, (GP66::kb_region_t) 0x13, ""},
    {0x00, 24, (GP66::kb_region_t) 0x13, ""},
    {0x00, 25, (GP66::kb_region_t) 0x13, ""},
    {0x00, 26, (GP66::kb_region_t) 0x13, ""},
    {0x00, 27, (GP66::kb_region_t) 0x13, ""},
    {0x00, 28, (GP66::kb_region_t) 0x13, ""},
    {0x00, 29, (GP66::kb_region_t) 0x13, ""},
    {0x00, 30, (GP66::kb_region_t) 0x13, ""},
    {0x00, 31, (GP66::kb_region_t) 0x13, ""},
    {0x00, 32, (GP66::kb_region_t) 0x13, ""},
    {0x00, 33, (GP66::kb_region_t) 0x13, ""},
    {0x00, 34, (GP66::kb_region_t) 0x13, ""},
    {0x00, 35, (GP66::kb_region_t) 0x13, ""},
    {0x00, 36, (GP66::kb_region_t) 0x13, ""},
    {0x00, 37, (GP66::kb_region_t) 0x13, ""},
    {0x00, 38, (GP66::kb_region_t) 0x13, ""},
    {0x00, 39, (GP66::kb_region_t) 0x13, ""},
    {0x00, 40, (GP66::kb_region_t) 0x13, ""},
    {0x00, 41, (GP66::kb_region_t) 0x13, ""},

    {0x29, 0, (GP66::kb_region_t) 0x18, "", KeyKind::Escape}, // escape
    {0x2a, 1, (GP66::kb_region_t) 0x18, ""}, // backspace
    {0x2b, 2, (GP66::kb_region_t) 0x18, ""}, // tab
    {0x2c, 3, (GP66::kb_region_t) 0x18, ""}, // space
    {0x2d, 4, (GP66::kb_region_t) 0x18, ""}, // -
    {0x2e, 5, (GP66::kb_region_t) 0x18, ""}, // =
    {0x2f, 6, (GP66::kb_region_t) 0x18, ""}, // [
    {0x30, 7, (GP66::kb_region_t) 0x18, ""}, // ]
    {0x33, 8, (GP66::kb_region_t) 0x18, ""}, // ;
    {0x34, 9, (GP66::kb_region_t) 0x18, ""}, // '
    {0x35, 10, (GP66::kb_region_t) 0x18, ""}, // `
    {0x36, 11, (GP66::kb_region_t) 0x18, ""}, // ,
    {0x37, 12, (GP66::kb_region_t) 0x18, ""}, // .
    {0x38, 13, (GP66::kb_region_t) 0x18, ""}, // /
    {0x39, 14, (GP66::kb_region_t) 0x18, ""}, // caps lock
    {0x66, 15, (GP66::kb_region_t) 0x18, ""}, // power button
    {0xe0, 16, (GP66::kb_region_t) 0x18, ""}, // left control
    {0xe1, 17, (GP66::kb_region_t) 0x18, ""}, // left shift
    {0xe2, 18, (GP66::kb_region_t) 0x18, ""}, // left alt
    {0xe3, 19, (GP66::kb_region_t) 0x18, ""}, // left super
    {0xe4, 20, (GP66::kb_region_t) 0x18, ""}, // right control
    {0xe5, 21, (GP66::kb_region_t) 0x18, ""}, // right shift
    {0xe6, 22, (GP66::kb_region_t) 0x18, ""}, // right alt
    {0xf0, 23, (GP66::kb_region_t) 0x18, ""}, // fn
    {0x00, 24, (GP66::kb_region_t) 0x18, ""}, //
    {0x00, 25, (GP66::kb_region_t) 0x18, ""},
    {0x00, 26, (GP66::kb_region_t) 0x18, ""},
    {0x00, 27, (GP66::kb_region_t) 0x18, ""},
    {0x00, 28, (GP66::kb_region_t) 0x18, ""},
    {0x00, 29, (GP66::kb_region_t) 0x18, ""},
    {0x00, 30, (GP66::kb_region_t) 0x18, ""},
    {0x00, 31, (GP66::kb_region_t) 0x18, ""},
    {0x00, 32, (GP66::kb_region_t) 0x18, ""},
    {0x00, 33, (GP66::kb_region_t) 0x18, ""},
    {0x00, 34, (GP66::kb_region_t) 0x18, ""},
    {0x00, 35, (GP66::kb_region_t) 0x18, ""},
    {0x00, 36, (GP66::kb_region_t) 0x18, ""},
    {0x00, 37, (GP66::kb_region_t) 0x18, ""},
    {0x00, 38, (GP66::kb_region_t) 0x18, ""},
    {0x00, 39, (GP66::kb_region_t) 0x18, ""},
    {0x00, 40, (GP66::kb_region_t) 0x18, ""},
    {0x00, 41, (GP66::kb_region_t) 0x18, ""},

    {0x04, 0, (GP66::kb_region_t) 0x2a, ""}, // a
    {0x05, 1, (GP66::kb_region_t) 0x2a, ""}, // b
    {0x06, 2, (GP66::kb_region_t) 0x2a, ""}, // c
    {0x07, 3, (GP66::kb_region_t) 0x2a, ""}, // d
    {0x08, 4, (GP66::kb_region_t) 0x2a, ""}, // e
    {0x09, 5, (GP66::kb_region_t) 0x2a, ""}, // f
    {0x0a, 6, (GP66::kb_region_t) 0x2a, ""}, // g
    {0x0b, 7, (GP66::kb_region_t) 0x2a, ""}, // h
    {0x0c, 8, (GP66::kb_region_t) 0x2a, ""}, // i
    {0x0d, 9, (GP66::kb_region_t) 0x2a, ""}, // j
    {0x0e, 10, (GP66::kb_region_t) 0x2a, ""}, // k
    {0x0f, 11, (GP66::kb_region_t) 0x2a, ""},  // l
    {0x10, 12, (GP66::kb_region_t) 0x2a, ""}, // m
    {0x11, 13, (GP66::kb_region_t) 0x2a, ""}, // n
    {0x12, 14, (GP66::kb_region_t) 0x2a, ""}, // o
    {0x13, 15, (GP66::kb_region_t) 0x2a, ""}, // p
    {0x14, 16, (GP66::kb_region_t) 0x2a, ""}, // q
    {0x15, 17, (GP66::kb_region_t) 0x2a, ""}, // r
    {0x16, 18, (GP66::kb_region_t) 0x2a, ""}, // s
    {0x17, 19, (GP66::kb_region_t) 0x2a, ""}, // t
    {0x18, 20, (GP66::kb_region_t) 0x2a, ""}, // u
    {0x19, 21, (GP66::kb_region_t) 0x2a, ""}, // v
    {0x1a, 22, (GP66::kb_region_t) 0x2a, ""}, // w
    {0x1b, 23, (GP66::kb_region_t) 0x2a, ""}, // x
    {0x1c, 24, (GP66::kb_region_t) 0x2a, ""}, // y
    {0x1d, 25, (GP66::kb_region_t) 0x2a, ""}, // z
    {0x1e, 26, (GP66::kb_region_t) 0x2a, ""}, // 1
    {0x1f, 27, (GP66::kb_region_t) 0x2a, ""}, // 2
    {0x20, 28, (GP66::kb_region_t) 0x2a, ""}, // 3
    {0x21, 29, (GP66::kb_region_t) 0x2a, ""}, // 4
    {0x22, 30, (GP66::kb_region_t) 0x2a, ""}, // 5
    {0x23, 31, (GP66::kb_region_t) 0x2a, ""}, // 6
    {0x24, 32, (GP66::kb_region_t) 0x2a, ""}, // 7
    {0x25, 33, (GP66::kb_region_t) 0x2a, ""}, // 8
    {0x26, 34, (GP66::kb_region_t) 0x2a, ""}, // 9
    {0x27, 35, (GP66::kb_region_t) 0x2a, ""}, // 0
    {0x3a, 36, (GP66::kb_region_t) 0x2a, ""}, // F1
    {0x3b, 37, (GP66::kb_region_t) 0x2a, ""}, // F2
    {0x3c, 38, (GP66::kb_region_t) 0x2a, ""}, // F3
    {0x3d, 39, (GP66::kb_region_t) 0x2a, ""}, // F4
    {0x3e, 40, (GP66::kb_region_t) 0x2a, ""}, // F5
    {0x3f, 41, (GP66::kb_region_t) 0x2a, ""}, // F6
  }};
};

}