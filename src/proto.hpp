//
// Created by jonathan on 22/04/22.
//

#pragma once

#include <cstdint>

#include <array>
#include <limits>
#include <tuple>

namespace mkb
{

namespace gp66
{

enum kb_region_t : uint8_t
{
  KB_REGION_A = 0x0b,
  KB_REGION_B = 0x13,
  KB_REGION_C = 0x18,
  KB_REGION_D = 0x2a,
};

constexpr size_t num_regions = 4;

inline constexpr std::array<size_t, std::numeric_limits<uint8_t>::max()> kb_region_map_init()
{
  std::array<size_t, std::numeric_limits<uint8_t>::max()> r {};
  for (auto& e: r)
    e = 0;

  r[KB_REGION_A] = 0;
  r[KB_REGION_B] = 1;
  r[KB_REGION_C] = 2;
  r[KB_REGION_D] = 3;

  return r;
}

inline constexpr std::array<kb_region_t, num_regions> kb_region_inverse_map_init()
{
  return {KB_REGION_A, KB_REGION_B, KB_REGION_C, KB_REGION_D};
}

using WritePacket = std::array<uint8_t, 65>;

inline constexpr WritePacket refresh_packet_a_init()
{
  WritePacket r {};
  for (auto& e: r)
    e = 0;
  r[1] = 0xd;
  r[3] = 0x2;
  r[std::tuple_size_v<WritePacket> - 1] = 0x8;
  return r;
}

inline constexpr WritePacket refresh_packet_b_init()
{
  WritePacket r {};
  for (auto& e: r)
    e = 0;
  r[1] = 0x9;
  r[std::tuple_size_v<WritePacket> - 1] = 0x8;
  return r;
}

}

struct GP66
{
  constexpr static unsigned short vendor_id = 0x1038;
  constexpr static unsigned short product_id = 0x113a;

  using kb_region_t = gp66::kb_region_t;
  constexpr static auto kb_region_map = gp66::kb_region_map_init();
  constexpr static auto kb_region_inverse_map = gp66::kb_region_inverse_map_init();

  constexpr static auto num_regions = gp66::num_regions;

  using WritePacket = gp66::WritePacket;
  constexpr static WritePacket refresh_packet_a = gp66::refresh_packet_a_init();
  constexpr static WritePacket refresh_packet_b = gp66::refresh_packet_b_init();

#pragma pack(push, 1)
  struct KeyRGB
  {
    uint8_t r {0};
    uint8_t g {0};
    uint8_t b {0};
  };

  struct KeyInfo
  {
    KeyRGB rgb;
    std::array<uint8_t, 8> idk {0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t key_id {0};
  };

  constexpr static std::array<uint8_t, 8> idk_set_colour {0x00, 0x00, 0x00, 0x2c, 0x01, 0x00, 0x01, 0x00};

  struct Packet
  {
    uint8_t hid_report_ {0x00};
    uint8_t h1 {0x0e};
    uint8_t pad0_ {0x00};
    kb_region_t kb_region;
    uint8_t pad1_ {0x00};
    std::array<KeyInfo, 42> keys;
  };
#pragma pack(pop)

};

}


