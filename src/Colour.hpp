//
// Created by jonathan on 22/04/22.
//

#pragma once

#include <cmath>
#include <cstddef>
#include <cstdint>

// This makes g++ be quiet about anonymous structs
#ifdef __GNUC__
#define EXTENSION_ANON_STRUCT __extension__
#else
#define EXTENSION_ANON_STRUCT
#endif

namespace mkb
{

struct Colour
{
  union
  {
    EXTENSION_ANON_STRUCT struct
    {
      union
      {
        EXTENSION_ANON_STRUCT struct
        {
          uint8_t r;
          uint8_t g;
          uint8_t b;
        };

        EXTENSION_ANON_STRUCT struct
        {
          uint8_t h;
          uint8_t s;
          uint8_t v;
        };
      };
      uint8_t a;
    };

    uint32_t hex_;
  };

  constexpr Colour(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 0xFF)
    : r(r), g(g), b(b), a(a)
  {}

  constexpr static uint32_t bswap_hex(const uint32_t i)
  {
#if (defined(_MSC_VER) && REG_DWORD == REG_DWORD_LITTLE_ENDIAN) || (defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__))
    constexpr bool is_le = true;
#else
    constexpr bool is_le = false;
#endif

    if constexpr (is_le) {
      return
        ((i >> 24U) & 0xFFU)
        | ((i >> 8U) & 0xFF00U)
        | ((i << 24U) & 0xFF000000U)
        | ((i << 8U) & 0xFF0000U);
    }

    return i;
  }

  constexpr explicit Colour(uint32_t hex)
    : hex_(bswap_hex(hex))
  {
  }

  [[nodiscard]] Colour hsv_to_rgb() const
  {
    const auto H = static_cast<uint16_t>(h);
    const auto S = static_cast<uint16_t>(s);
    const auto V = static_cast<uint16_t>(v);

    const auto C = (V * S) / 255;
    const auto Hp = H / (255 / 6);
    const auto X = C * (1 - std::abs(Hp % 2 - 1));
    const auto m = V - C;

    const auto colour = [&] {
      switch (Hp) {
        case 0:
          return Colour(C, X, 0, a);
        case 1:
          return Colour(X, C, 0, a);
        case 2:
          return Colour(0, C, X, a);
        case 3:
          return Colour(0, X, C, a);
        case 4:
          return Colour(X, 0, C, a);
        case 5:
          return Colour(C, 0, X, a);
        default:
          return Colour(0, 0, 0, a);
      }
    }();

    const auto r = static_cast<uint16_t>(colour.r) + m;
    const auto g = static_cast<uint16_t>(colour.g) + m;
    const auto b = static_cast<uint16_t>(colour.b) + m;

    return Colour(r, g, b, a);
  }

//  template<typename Ar>
//  void serialize(Ar& ar)
//  {
//    ar(CEREAL_NVP(r));
//    ar(CEREAL_NVP(g));
//    ar(CEREAL_NVP(mkb));
//    ar(CEREAL_NVP(a));
//  }

  bool operator!=(const Colour& other) const
  {
    return hex_ != other.hex_;
  }

  bool operator==(const Colour& other) const
  {
    return !(this->operator!=(other));
  }
};

// paranoia
static_assert(sizeof(Colour) == sizeof(uint32_t));
static_assert(offsetof(Colour, r) == 0);
static_assert(offsetof(Colour, h) == 0);
static_assert(offsetof(Colour, g) == 1);
static_assert(offsetof(Colour, s) == 1);
static_assert(offsetof(Colour, b) == 2);
static_assert(offsetof(Colour, v) == 2);
static_assert(offsetof(Colour, a) == 3);
static_assert(offsetof(Colour, hex_) == 0);

namespace colours
{

constexpr static Colour white = Colour(0xFFFFFFFF);
constexpr static Colour black = Colour(0x000000FF);
constexpr static Colour red = Colour(0xFF0000FF);
constexpr static Colour green = Colour(0x00FF00FF);
constexpr static Colour blue = Colour(0x0000FFFF);
constexpr static Colour magenta = Colour(0xFF00FFFF);

}

} // namespace mkb

#undef EXTENSION_ANON_STRUCT
