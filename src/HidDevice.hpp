//
// Created by jonathan on 22/04/22.
//

#pragma once

#include "exception.hpp"

#include <hidapi.h>

#include <sys/stat.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>
#include <vector>
#include <filesystem>


namespace mkb
{

DEFINE_RUNTIME_EXCEPTION(HidNoSuchDevice)

DEFINE_RUNTIME_EXCEPTION(HidOpenFailed)

DEFINE_RUNTIME_EXCEPTION(HidWriteFailed)

// hid_device_info but c++ allocs
struct HidDeviceInfo
{
  std::filesystem::path path;
  unsigned short vendor_id;
  unsigned short product_id;
  std::wstring serial_number;
  unsigned short release_number;
  std::wstring manufacturer_string;
  std::wstring product_string;
  unsigned short usage_page;
  unsigned short usage;
  int interface_number;
};

inline std::string to_string(const wchar_t* str)
{
  std::wstring wstr(str);
  return {wstr.begin(), wstr.end()};
}

class HidDevice
{
public:
  HidDevice(unsigned short vendor_id, unsigned short product_id)
  {
    const auto devices = enumerate_device(vendor_id, product_id);

    std::cout << "Found " << devices.size() << " matching devices\n";
    for (const auto& dev: devices)
      std::cout << "  " << dev.path << '\n';

    if (devices.empty())
      throw HidNoSuchDevice("No compatible device found.");

    const auto device_info_it = std::find_if(
      devices.begin(), devices.end(),
      [](const auto& dev) {
        return 0 == dev.interface_number;
      });

    if (device_info_it == devices.end())
      throw HidNoSuchDevice("Device found but could not find the correct interface.");

    const auto& device_info = *device_info_it;

    const auto has_perms = [&] {
      using perms = std::filesystem::perms;
      const auto permissions = std::filesystem::status(device_info.path).permissions();

      constexpr auto world_set = perms::others_read | perms::others_write;
      constexpr auto group_set = perms::group_read | perms::group_write;
      constexpr auto owner_set = perms::owner_read | perms::owner_write;

      if ((bool) (permissions & world_set))
        return true;

      struct stat info {};
      if (-1 == ::stat(device_info.path.c_str(), &info))
        throw std::system_error(errno, std::system_category());

      if (info.st_uid == getuid() || info.st_uid == geteuid()) {
        // We are owner of the device file
        if ((bool) (permissions & owner_set))
          return true;

      } else if (const auto num_groups = getgroups(0, nullptr); num_groups >= 0) {
        // check if we are in a group with the device file

        std::vector<gid_t> groups(num_groups);
        if (num_groups)
          getgroups(num_groups, groups.data());

        groups.emplace_back(geteuid());

        if (std::find(groups.begin(), groups.end(), info.st_gid) != groups.end()) {
          if ((bool) (permissions & group_set))
            return true;
        }
      }

      return false;
    }();

    if (!has_perms)
      throw HidOpenFailed(std::string {"Do not have sufficient permissions for device "} + device_info.path.string());

    std::cout << "opening device " << std::hex << vendor_id << ':' << product_id << std::dec << " on " << device_info.path << '\n';
    m_hid = hid_open(vendor_id, product_id, device_info.serial_number.c_str());

    if (!m_hid)
      throw HidOpenFailed("Failed to open device for unknown reason");
  }

  ~HidDevice()
  {
    if (m_hid)
      hid_close(m_hid);
  }

  HidDevice(const HidDevice&) = delete;

  HidDevice& operator=(const HidDevice&) = delete;

  HidDevice(HidDevice&& other) noexcept
    : m_hid(other.m_hid)
  {
    other.m_hid = nullptr;
  }

  HidDevice& operator=(HidDevice&& other) noexcept
  {
    if (this != &other) {
      m_hid = other.m_hid;
      other.m_hid = nullptr;
    }
    return *this;
  }

  void write(const void* data, size_t size)
  {
    const auto wrote_bytes = hid_write(m_hid, reinterpret_cast<const unsigned char*>(data), size);
    if (-1 == wrote_bytes)
      throw HidWriteFailed(to_string(hid_error(m_hid)));
    if (wrote_bytes < size)
      throw HidWriteFailed("Insufficient bytes wrote");
  }

  void send_feature_report(const void* data, size_t size)
  {
    const auto wrote_bytes = hid_send_feature_report(m_hid, reinterpret_cast<const unsigned char*>(data), size);
    if (-1 == wrote_bytes)
      throw HidWriteFailed(to_string(hid_error(m_hid)));
    if (wrote_bytes < size)
      throw HidWriteFailed("Insufficient bytes wrote");
  }

  static std::vector<HidDeviceInfo> enumerate_device(unsigned short vendor_id, unsigned short product_id)
  {
    hid_device_info* device_ptr = hid_enumerate(vendor_id, product_id);
    std::vector<HidDeviceInfo> ret;

    hid_device_info* device_it = device_ptr;
    while (device_it) {
      HidDeviceInfo info {
        .path=device_it->path,
        .vendor_id=device_it->vendor_id,
        .product_id=device_it->product_id,
        .serial_number=device_it->serial_number,
        .release_number=device_it->release_number,
        .manufacturer_string=device_it->manufacturer_string,
        .product_string=device_it->product_string,
        .usage_page=device_it->usage_page,
        .usage=device_it->usage,
        .interface_number=device_it->interface_number
      };
      ret.emplace_back(info);

      device_it = device_it->next;
    }

    hid_free_enumeration(device_ptr);

    return ret;
  }

  static bool has_device(unsigned short vendor_id, unsigned short product_id)
  {
    return !enumerate_device(vendor_id, product_id).empty();
  }

private:
  hid_device* m_hid {nullptr};
};


} // namespace mkb
