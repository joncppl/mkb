#include "KbControllerInterface.hpp"

#include <popl.hpp>

using namespace mkb;
using namespace popl;

static uint8_t parse_half_hex(char c)
{
  if (c >= 'a') return c - 'a' + 10;
  if (c >= 'A') return c - 'A' + 10;
  return c - '0';
}

static uint8_t parse_hex(const char* c)
{
  return parse_half_hex(*c) << 4 | parse_half_hex(*(c + 1));
}

static Colour parse_colour(const std::string& str)
{
  // TODO: could probably check against some named colours

  if (str.size() == 3) {
    const auto r = (uint32_t) parse_half_hex(str[0]) & 0xf;
    const auto g = (uint32_t) parse_half_hex(str[1]) & 0xf;
    const auto b = (uint32_t) parse_half_hex(str[2]) & 0xf;
    return Colour(r << 28 | r << 24 | g << 20 | g << 16 | b << 12 | b << 8 | 0xff);
  }

  if (str.size() == 6) {
    const auto r = (uint32_t) parse_hex(str.c_str() + 0) & 0xff;
    const auto g = (uint32_t) parse_hex(str.c_str() + 2) & 0xff;
    const auto b = (uint32_t) parse_hex(str.c_str() + 4) & 0xff;
    return Colour(r << 24 | g << 16 | b << 8 | 0xff);
  }

  return colours::black;
}

int main(int argc, char* argv[])
{
  OptionParser opts("Usage: mk");

  auto help_option = opts.add<Switch>("h", "help", "print help");
  auto id_option = opts.add<Value<int>>("i", "key-id", "key id for operation");
  auto colour_option = opts.add<Value<std::string>>("c", "colour", "colour for operation");
  auto comp_option = opts.add<Switch, Attribute::hidden>("z", "completions", "completions");

  opts.parse(argc, argv);

  if (help_option->count() > 0) {
    std::cout << opts.description() << '\n';
    std::cout << opts << '\n';
    return 0;
  }

  if (comp_option->count() > 0) {
    BashCompletionOptionPrinter printer(&opts, "mkbcli");
    std::cout << printer.print() << '\n';
    return 0;
  }

  enum struct Mode
  {
    Unspecified,
    One,
    All
  };

  Mode mode {Mode::Unspecified};

  const auto positionals = opts.non_option_args();
  for (const auto& arg: positionals) {
    if (arg == "one") {
      mode = Mode::One;
      break;
    } else if (arg == "all") {
      mode = Mode::All;
      break;
    } else {
      std::cerr << "unknown operation: " << arg << '\n';
      return 1;
    }

  }

  auto device = KbControllerInterface::create();
  switch (mode) {
    case Mode::Unspecified:
      std::cerr << "no operation specified" << std::endl;
      return 1;
    case Mode::One:
      device->set_one_key_to_colour_by_id(id_option->value(), parse_colour(colour_option->value()));
      return 0;
    case Mode::All:
      device->set_all_keys_to_colour(parse_colour(colour_option->value()));
      break;
  }
}
