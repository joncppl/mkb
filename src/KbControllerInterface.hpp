//
// Created by jonathan on 22/04/22.
//

#pragma once

#include "Colour.hpp"
#include "KeyState.hpp"

#include <memory>

namespace mkb
{

class KbControllerInterface
{
public:
  static std::unique_ptr<KbControllerInterface> create();

  virtual void set_all_keys_to_colour(Colour colour) = 0;

  /// The id is MSI/SS's internal ID, so this is primarily used for mapping keys
  virtual void set_one_key_to_colour_by_id(uint8_t id, Colour colour) = 0;

  virtual void set_key_state(const KeyState& state) = 0;

public:

  virtual ~KbControllerInterface() = default;

  KbControllerInterface(const KbControllerInterface&) = delete;

  KbControllerInterface& operator=(const KbControllerInterface&) = delete;

  KbControllerInterface(KbControllerInterface&&) = delete;

  KbControllerInterface& operator=(KbControllerInterface&&) = delete;

protected:
  KbControllerInterface() = default;
};

}
