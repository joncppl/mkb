//
// Created by jonathan on 22/04/22.
//

#include "KbControllerInterface.hpp"

#include "HidDevice.hpp"
#include "proto.hpp"
#include "key_defs.hpp"

#include <thread>

namespace mkb
{

template<typename Proto>
using GenericRegionPackets = std::array<typename Proto::Packet, Proto::num_regions>;

template<typename Proto>
constexpr static GenericRegionPackets<Proto> init_region_packets()
{
  GenericRegionPackets<Proto> r {};
  for (size_t i = 0; i < Proto::num_regions; i++)
    r[i].kb_region = Proto::kb_region_inverse_map[i];
  return r;
}

template<typename Proto, typename Keys>
class KbController final : public KbControllerInterface
{
  using RegionPackets = GenericRegionPackets<Proto>;
  constexpr static auto initial_region_packets {init_region_packets<Proto>()};

public:
  explicit KbController()
    : m_dev(Proto::vendor_id, Proto::product_id),
      m_key_map(Keys::keys)
  {}

  ~KbController() final = default;

  void set_all_keys_to_colour(Colour colour) final
  {
    auto region_packets = initial_region_packets;

    process_all_keys(region_packets, [&](const KeyDefinition<Proto>& key, typename Proto::KeyInfo& key_info) {
      key_info.key_id = key.key_id;
      key_info.idk = Proto::idk_set_colour;
      key_info.rgb.r = colour.r;
      key_info.rgb.g = colour.g;
      key_info.rgb.b = colour.b;
    });

    write_regions(region_packets);
    refresh();
  }

  void set_one_key_to_colour_by_id(uint8_t id, Colour colour) final
  {
    auto region_packets = initial_region_packets;

    process_all_keys(region_packets, [&](const KeyDefinition<Proto>& key, typename Proto::KeyInfo& key_info) {
      key_info.key_id = key.key_id;
      key_info.idk = Proto::idk_set_colour;
      if (id == key.key_id) {
        key_info.rgb.r = colour.r;
        key_info.rgb.g = colour.g;
        key_info.rgb.b = colour.b;
      } else {
        key_info.rgb.r = 0;
        key_info.rgb.g = 0;
        key_info.rgb.b = 0;
      }
    });

    write_regions(region_packets);
    refresh();
  }

  void set_key_state(const KeyState& state) final
  {
    auto region_packets = initial_region_packets;

    process_all_keys(region_packets, [&](const KeyDefinition<Proto>& key, typename Proto::KeyInfo& key_info) {
      key_info.key_id = key.key_id;
      key_info.idk = Proto::idk_set_colour;
      const auto& per_key_state = state.get_key_state(key.key_kind);

      key_info.rgb.r = per_key_state.colour.r;
      key_info.rgb.g = per_key_state.colour.g;
      key_info.rgb.b = per_key_state.colour.b;
    });

    write_regions(region_packets);
    refresh();
  }

private:
  template<typename Func>
  void process_all_keys(RegionPackets& region_packets, Func&& func)
  {
    for (const auto& key: Keys::keys) {
      const auto region_packets_index = Proto::kb_region_map[key.region];
      if (region_packets_index >= Proto::num_regions)
        continue;

      if (key.spot_in_region >= region_packets[region_packets_index].keys.size())
        continue;

      auto& key_info = region_packets[region_packets_index].keys[key.spot_in_region];

      func(key, key_info);
    }
  }

  void write_region(const typename Proto::Packet& packet)
  {
    m_dev.send_feature_report(&packet, sizeof(packet));
  }

  void write_regions(const RegionPackets& region_packets)
  {
    for (const auto& region: region_packets) {
      write_region(region);
      // NB: It appears that sending packets with no delay results in corruption
      //  (ie. RNG which keys take on the new state / some packets are "ignored")
      //  USB traces from SS's windows app show about a ~20ms delay between packets,
      //  but I've found that just 1ms is enough
      std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }
  }

  void refresh()
  {
    m_dev.write(Proto::refresh_packet_a.data(), Proto::refresh_packet_a.size());
    m_dev.write(Proto::refresh_packet_b.data(), Proto::refresh_packet_b.size());
  }

  HidDevice m_dev;
  const KeyMap<Proto> m_key_map;
};

std::unique_ptr<KbControllerInterface> KbControllerInterface::create()
{
  // TODO: enumerate and find compatible devices,
  //  when more than just the one device is supported ^^

  return std::make_unique<KbController<GP66, gp66_keys>>();
}

}