//
// Created by jonathan on 23/04/22.
//

#pragma once

#include <cstdint>
#include <cstddef>

#include <array>
#include <string_view>

namespace mkb
{

enum struct KeyKind
{
  None,
  Escape,
  F1,
  F2,
  F3,
  F4,
  F5,
  F6,
  F7,
  F8,
  F9,
  F10,
  F11,
  F12,
  Delete,
  Power,
  BackTick,
  One,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Zero,
  Minus,
  Equal,
  BackSpace,
  Home,
  Tab,
  Q,
  W,
  E,
  R,
  T,
  Y,
  U,
  I,
  O,
  P,
  LeftBracket,
  RightBracket,
  BigBackSlash, // TODO
  PageUp,
  CapsLock,
  A,
  S,
  D,
  F,
  G,
  H,
  J,
  K,
  L,
  SemiColon,
  Quote,
  Enter,
  PageDown,
  LeftShift,
  Z,
  X,
  C,
  V,
  B,
  N,
  M,
  Comma,
  Period,
  ForwardSlash,
  RightShift,
  UpArrow,
  End,
  LeftControl,
  LeftSuper,
  LeftAlt,
  Space,
  RightAlt,
  SmallBackSlack, // TODO
  Fn,
  RightControl,
  LeftArrow,
  DownArrow,
  RightArrow
};

constexpr size_t num_key_kinds = static_cast<size_t>(KeyKind::RightArrow) + 1;

using KeyStringMap = std::array<std::string_view, num_key_kinds>;

constexpr KeyStringMap key_string_map_init()
{
  KeyStringMap ret {};
  for (size_t i = 0; i < num_key_kinds; i++)
    ret[i] = "null";

  const auto m = [&ret](KeyKind k, const std::string_view& s) {
    ret[static_cast<size_t>(k)] = s;
  };

  m(KeyKind::None, "none");
  m(KeyKind::Escape, "Esc");
  m(KeyKind::F1, "F1");
  m(KeyKind::F2, "F2");
  m(KeyKind::F3, "F3");
  m(KeyKind::F4, "F4");
  m(KeyKind::F5, "F5");
  m(KeyKind::F6, "F6");
  m(KeyKind::F7, "F7");
  m(KeyKind::F8, "F8");
  m(KeyKind::F9, "F9");
  m(KeyKind::F10, "F10");
  m(KeyKind::F11, "F11");
  m(KeyKind::F12, "F12");
  m(KeyKind::Delete, "Delete");
  m(KeyKind::Power, "Power");
  m(KeyKind::BackTick, "BackTick");
  m(KeyKind::One, "One");
  m(KeyKind::Two, "Two");
  m(KeyKind::Three, "Three");
  m(KeyKind::Four, "Four");
  m(KeyKind::Five, "Five");
  m(KeyKind::Six, "Six");
  m(KeyKind::Seven, "Seven");
  m(KeyKind::Eight, "Eight");
  m(KeyKind::Nine, "Nine");
  m(KeyKind::Zero, "Zero");
  m(KeyKind::Minus, "Minus");
  m(KeyKind::Equal, "Equal");
  m(KeyKind::BackSpace, "BackSpace");
  m(KeyKind::Home, "Home");
  m(KeyKind::Tab, "Tab");
  m(KeyKind::Q, "Q");
  m(KeyKind::W, "W");
  m(KeyKind::E, "E");
  m(KeyKind::R, "R");
  m(KeyKind::T, "T");
  m(KeyKind::Y, "Y");
  m(KeyKind::U, "U");
  m(KeyKind::I, "I");
  m(KeyKind::O, "O");
  m(KeyKind::P, "P");
  m(KeyKind::LeftBracket, "LeftBracket");
  m(KeyKind::RightBracket, "RightBracket");
  m(KeyKind::BigBackSlash, "BigBackSlash");
  m(KeyKind::PageUp, "PageUp");
  m(KeyKind::CapsLock, "CapsLock");
  m(KeyKind::A, "A");
  m(KeyKind::S, "S");
  m(KeyKind::D, "D");
  m(KeyKind::F, "F");
  m(KeyKind::G, "G");
  m(KeyKind::H, "H");
  m(KeyKind::J, "J");
  m(KeyKind::K, "K");
  m(KeyKind::L, "L");
  m(KeyKind::SemiColon, "SemiColon");
  m(KeyKind::Quote, "Quote");
  m(KeyKind::Enter, "Enter");
  m(KeyKind::PageDown, "PageDown");
  m(KeyKind::LeftShift, "LeftShift");
  m(KeyKind::Z, "Z");
  m(KeyKind::X, "X");
  m(KeyKind::C, "C");
  m(KeyKind::V, "V");
  m(KeyKind::B, "B");
  m(KeyKind::N, "N");
  m(KeyKind::M, "M");
  m(KeyKind::Comma, "Comma");
  m(KeyKind::Period, "Period");
  m(KeyKind::ForwardSlash, "ForwardSlash");
  m(KeyKind::RightShift, "RightShift");
  m(KeyKind::UpArrow, "UpArrow");
  m(KeyKind::End, "End");
  m(KeyKind::LeftControl, "LeftControl");
  m(KeyKind::LeftSuper, "LeftSuper");
  m(KeyKind::LeftAlt, "LeftAlt");
  m(KeyKind::Space, "Space");
  m(KeyKind::RightAlt, "RightAlt");
  m(KeyKind::SmallBackSlack, "SmallBackSlack");
  m(KeyKind::Fn, "Fn");
  m(KeyKind::RightControl, "RightControl");
  m(KeyKind::LeftArrow, "LeftArrow");
  m(KeyKind::DownArrow, "DownArrow");
  m(KeyKind::RightArrow, "RightArrow");
  return ret;
}

constexpr auto key_string_map {key_string_map_init()};

constexpr const char* KeyKind_str(KeyKind kind)
{
  return key_string_map[static_cast<size_t>(kind)].data();
}

}
