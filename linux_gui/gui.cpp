//
// Created by jonathan on 23/04/22.
//

#include "../src/KbControllerInterface.hpp"

#include <gtkmm/application.h>
#include <gtkmm/window.h>
#include <gtkmm/colorbutton.h>
#include <gtkmm/box.h>
#include <gtkmm/checkbutton.h>
#include <gtkmm/builder.h>
#include <gtkmm/fixed.h>
#include <gtkmm/label.h>

#include <iostream>

struct KeyDef
{
  mkb::KeyKind key_kind;
  int width;
  int height;
};

constexpr auto keyboard_definition = std::make_tuple(
  std::make_tuple(
    KeyDef {mkb::KeyKind::Escape, 50, 25},
    KeyDef {mkb::KeyKind::F1, 50, 25},
    KeyDef {mkb::KeyKind::F2, 50, 25},
    KeyDef {mkb::KeyKind::F3, 50, 25},
    KeyDef {mkb::KeyKind::F4, 50, 25},
    KeyDef {mkb::KeyKind::F5, 50, 25},
    KeyDef {mkb::KeyKind::F6, 50, 25},
    KeyDef {mkb::KeyKind::F7, 50, 25},
    KeyDef {mkb::KeyKind::F8, 50, 25},
    KeyDef {mkb::KeyKind::F9, 50, 25},
    KeyDef {mkb::KeyKind::F10, 50, 25},
    KeyDef {mkb::KeyKind::F11, 50, 25},
    KeyDef {mkb::KeyKind::F12, 50, 25},
    KeyDef {mkb::KeyKind::Delete, 50, 25},
    KeyDef {mkb::KeyKind::Power, 50, 25}
  ),
  std::make_tuple(
    KeyDef {mkb::KeyKind::BackTick, 25, 50},
    KeyDef {mkb::KeyKind::One, 50, 50},
    KeyDef {mkb::KeyKind::Two, 50, 50},
    KeyDef {mkb::KeyKind::Three, 50, 50},
    KeyDef {mkb::KeyKind::Four, 50, 50},
    KeyDef {mkb::KeyKind::Five, 50, 50},
    KeyDef {mkb::KeyKind::Six, 50, 50},
    KeyDef {mkb::KeyKind::Seven, 50, 50},
    KeyDef {mkb::KeyKind::Eight, 50, 50},
    KeyDef {mkb::KeyKind::Nine, 50, 50},
    KeyDef {mkb::KeyKind::Zero, 50, 50},
    KeyDef {mkb::KeyKind::Minus, 50, 50},
    KeyDef {mkb::KeyKind::Equal, 50, 50},
    KeyDef {mkb::KeyKind::BackSpace, 75, 50},
    KeyDef {mkb::KeyKind::Home, 50, 50}
  ),
  std::make_tuple(
    KeyDef {mkb::KeyKind::Tab, 55, 50},
    KeyDef {mkb::KeyKind::Q, 50, 50},
    KeyDef {mkb::KeyKind::W, 50, 50},
    KeyDef {mkb::KeyKind::E, 50, 50},
    KeyDef {mkb::KeyKind::R, 50, 50},
    KeyDef {mkb::KeyKind::T, 50, 50},
    KeyDef {mkb::KeyKind::Y, 50, 50},
    KeyDef {mkb::KeyKind::U, 50, 50},
    KeyDef {mkb::KeyKind::I, 50, 50},
    KeyDef {mkb::KeyKind::O, 50, 50},
    KeyDef {mkb::KeyKind::P, 50, 50},
    KeyDef {mkb::KeyKind::LeftBracket, 50, 50},
    KeyDef {mkb::KeyKind::RightBracket, 50, 50},
    KeyDef {mkb::KeyKind::BigBackSlash, 55, 50},
    KeyDef {mkb::KeyKind::PageUp, 50, 50}
  ),
  std::make_tuple(
    KeyDef {mkb::KeyKind::CapsLock, 50, 50},
    KeyDef {mkb::KeyKind::A, 50, 50},
    KeyDef {mkb::KeyKind::S, 50, 50},
    KeyDef {mkb::KeyKind::D, 50, 50},
    KeyDef {mkb::KeyKind::F, 50, 50},
    KeyDef {mkb::KeyKind::G, 50, 50},
    KeyDef {mkb::KeyKind::H, 50, 50},
    KeyDef {mkb::KeyKind::J, 50, 50},
    KeyDef {mkb::KeyKind::K, 50, 50},
    KeyDef {mkb::KeyKind::L, 50, 50},
    KeyDef {mkb::KeyKind::SemiColon, 50, 50},
    KeyDef {mkb::KeyKind::Quote, 50, 50},
    KeyDef {mkb::KeyKind::Enter, 50, 50},
    KeyDef {mkb::KeyKind::PageDown, 50, 50}
  ),
  std::make_tuple(
    KeyDef {mkb::KeyKind::LeftShift, 50, 50},
    KeyDef {mkb::KeyKind::Z, 50, 50},
    KeyDef {mkb::KeyKind::X, 50, 50},
    KeyDef {mkb::KeyKind::C, 50, 50},
    KeyDef {mkb::KeyKind::V, 50, 50},
    KeyDef {mkb::KeyKind::B, 50, 50},
    KeyDef {mkb::KeyKind::N, 50, 50},
    KeyDef {mkb::KeyKind::M, 50, 50},
    KeyDef {mkb::KeyKind::Comma, 50, 50},
    KeyDef {mkb::KeyKind::Period, 50, 50},
    KeyDef {mkb::KeyKind::ForwardSlash, 50, 50},
    KeyDef {mkb::KeyKind::RightShift, 50, 50},
    KeyDef {mkb::KeyKind::UpArrow, 50, 50},
    KeyDef {mkb::KeyKind::End, 50, 50}
  ),
  std::make_tuple(
    KeyDef {mkb::KeyKind::LeftControl, 50, 50},
    KeyDef {mkb::KeyKind::LeftSuper, 50, 50},
    KeyDef {mkb::KeyKind::LeftAlt, 50, 50},
    KeyDef {mkb::KeyKind::Space, 50, 50},
    KeyDef {mkb::KeyKind::RightAlt, 50, 50},
    KeyDef {mkb::KeyKind::SmallBackSlack, 50, 50},
    KeyDef {mkb::KeyKind::Fn, 50, 50},
    KeyDef {mkb::KeyKind::RightControl, 50, 50},
    KeyDef {mkb::KeyKind::LeftArrow, 50, 50},
    KeyDef {mkb::KeyKind::DownArrow, 50, 50},
    KeyDef {mkb::KeyKind::RightArrow, 50, 50}
  )
);

static inline mkb::Colour gdk_colour_to_mdk(const Gdk::Color& c)
{
  return mkb::Colour {
    static_cast<uint8_t>(c.get_red()),
    static_cast<uint8_t>(c.get_green()),
    static_cast<uint8_t>(c.get_blue()),
  };
}

using KeyStateCallback = std::function<void(mkb::KeyKind, const mkb::PerKeyState&)>;
using AllKeyStateCallback = std::function<void(const mkb::PerKeyState&)>;

class KeyboardDisplay : public Gtk::Fixed
{
public:
  explicit KeyboardDisplay(KeyStateCallback key_state_callback)
    : Fixed(),
      m_key_state_callback(std::move(key_state_callback))
  {
    set_hexpand(false);
    set_vexpand(false);
    set_halign(Gtk::ALIGN_CENTER);
    set_vexpand(Gtk::ALIGN_CENTER);

    setup_row(keyboard_definition);
  }

  void set_colour(const Gdk::Color& c)
  {
    for (auto& pair: m_keys)
      pair.first.set_color(c);
  }

private:
  int cursor_x {0};
  int cursor_y {0};
  int row_height {0};

  template<typename Keys, size_t I = 0>
  void setup_key(const Keys& keys)
  {
    const KeyDef& def = std::get<I>(keys);

    Gtk::ColorButton button;
    button.show();
    button.set_size_request(def.width, def.height);

    // try to place the label on top of the button.
    //  is this reliable?
    Gtk::Label label(mkb::KeyKind_str(def.key_kind));
    label.show();
    label.set_padding(0, 0);
    label.set_margin_bottom(0);
    label.set_margin_top(0);
    label.set_margin_left(0);
    label.set_margin_right(0);
    label.set_margin_end(0);
    label.set_margin_start(0);
    label.set_size_request(def.width, def.height);
    label.set_justify(Gtk::JUSTIFY_CENTER);

    const auto height = def.height + 6;
    if (height > row_height)
      row_height = height;

    put(button, cursor_x, cursor_y);
    put(label, cursor_x, cursor_y);
    cursor_x += def.width;

    button.signal_color_set().connect([this, kind = def.key_kind, index = m_keys.size()]() {
      mkb::PerKeyState key_state {
        .colour=gdk_colour_to_mdk(m_keys[index].first.get_color())
      };
      m_key_state_callback(kind, key_state);
    });

    m_keys.emplace_back(std::make_pair(std::move(button), std::move(label)));

    if constexpr(I + 1 < std::tuple_size_v<Keys>)
      setup_key<Keys, I + 1>(keys);
  }

  template<typename Rows, size_t I = 0>
  void setup_row(const Rows& rows)
  {
    setup_key(std::get<I>(rows));

    cursor_x = 0;
    cursor_y += row_height;
    row_height = 0;

    if constexpr(I + 1 < std::tuple_size_v<Rows>)
      setup_row<Rows, I + 1>(rows);
  }

  std::vector<std::pair<Gtk::ColorButton, Gtk::Label>> m_keys;
  KeyStateCallback m_key_state_callback;
};

class MainWindow : public Gtk::Window
{
public:
  MainWindow(
    Gtk::Window::BaseObjectType* cobject,
    const Glib::RefPtr<Gtk::Builder>& builder,
    const KeyStateCallback& key_state_callback,
    AllKeyStateCallback all_key_state_callback
  ) : Window(cobject),
      m_keyboard(key_state_callback),
      m_key_state_callback(key_state_callback),
      m_all_key_state_callback(std::move(all_key_state_callback))
  {
    builder->get_widget("box", m_box);
    builder->get_widget("all_check", m_all_check);
    builder->get_widget("all_colour", m_all_colour);
    m_box->add(m_keyboard);
    m_keyboard.show();

    m_all_colour->signal_color_set().connect([this]() {
      const auto colour = m_all_colour->get_color();
      m_keyboard.set_colour(colour);
      m_all_key_state_callback(mkb::PerKeyState {
        .colour=gdk_colour_to_mdk(colour)
      });
    });
  }

private:
  Gtk::Box* m_box {nullptr};
  KeyboardDisplay m_keyboard;
  Gtk::CheckButton* m_all_check {nullptr};
  Gtk::ColorButton* m_all_colour {nullptr};
  KeyStateCallback m_key_state_callback;
  AllKeyStateCallback m_all_key_state_callback;
};

class MKBGui : public Gtk::Application
{
public:
  MKBGui()
    : Gtk::Application("org.mkbgui")
  {
    signal_activate().connect([this]() { on_app_activate(); });
  }

private:
  void on_app_activate()
  {
    auto builder = Gtk::Builder::create_from_resource("/org/mkb/window.glade");
    builder->get_widget_derived<MainWindow>("MainWindow",
      m_window,
      [this](auto&& ...args) { set_key_state(args...); },
      [this](auto&& ...args) { set_all_state(args...); }
    );
    add_window(*m_window);
    m_window->show();

    try {
      m_controller = mkb::KbControllerInterface::create();
    } catch (const std::exception& e) {
      std::cerr << e.what() << '\n';
      // TODO: someway to show connection status in GUI
    }
  }

  void set_key_state(mkb::KeyKind kind, const mkb::PerKeyState& state)
  {
    m_key_state.set_key_state(kind, state);
    update_controller();
  }

  void set_all_state(const mkb::PerKeyState& state)
  {
    m_key_state.set_all(state);
    update_controller();
  }

  void update_controller()
  {
    if (m_controller)
      m_controller->set_key_state(m_key_state);
  }

  MainWindow* m_window {nullptr};
  std::unique_ptr<mkb::KbControllerInterface> m_controller;
  mkb::KeyState m_key_state;
};

int main(int argc, char* argv[])
{
  return (MKBGui()).run(argc, argv);
}