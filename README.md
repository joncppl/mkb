# MKB

RGB LED Control for MSI Vector GP66 Laptop

## Install

source only, assuming ubuntu~ish 

```bash
# get dependencies
sudo apt update -y && \
    sudo apt install -y git build-essential cmake libhidapi-dev libgtkmm-3.0-dev 

# get code
git clone https://gitlab.com/joncppl/mkb

# compile
mkdir -p mkb/build && cd mkb/build
cmake .. -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_PREFIX_PATH=/usr/local
make -j

# (optionally) install
sudo make install
```

### UDEV

```bash
sudo cp ./99-mkb.rules /etc/udev/rules.d
sudo udevadm control --reload && sudo udevadm trigger
```


## Usage

### Run The GUI:

```bash
mkbgui
```

### Run The CLI:

```bash
mkbcli
```

```
Usage: mk [options] mode
Usage: mk:
  -h, --help         print help
  -i, --key-id arg   key id for operation
  -c, --colour arg   colour for operation

mode is either:
  all  set all keys to colour
  one  set key defined by --key-id to specified colour
```

eg. Set all keys to red:
```
mkbcli -c F00 all
```

# TODOs:

- better GUI
- gui desktop file
- load/save config
- udev?
- run on startup (systemd?) resume from suspend (pm?)
- install (cpack?)
